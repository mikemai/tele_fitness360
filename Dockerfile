FROM python:3.7-slim

RUN mkdir -p /main_app /root/.py_package

COPY .py_package /root/.py_package
RUN pip install --no-cache-dir -r /root/.py_package/requirements.txt

COPY main_app /main_app
WORKDIR /main_app
CMD python telegram_app.py
# import io
import datetime
import logging
import numpy as np
import pandas as pd
import re
import sqlite3

from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler, RegexHandler)
from threading import Thread

from ex_function import (trainees_full_list, main_full_list, input_to_df,
                         myinfo, check_nric, refresh_creds, df_trainees_points)
from tasks import (app, task_add_data, task_add_tele_id, task_del_data, task_up_all_db)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)
TARGET_USER = 'main'


def run_celery():
    app.worker_main(['tasks', '-B', '--concurrency=1', '--loglevel=info'])
    # app.worker_main(['tasks', '--concurrency=1', '--loglevel=info'])


def msg_start(update, context):
    task_up_all_db.delay(TARGET_USER)

    txt = ''
    if TARGET_USER == 'main':
        txt = 'Signal Fitness 360'
    elif TARGET_USER == 'trainees':
        txt = 'Self-Regulated Fitness Programme'

    update.message.reply_text(text='Welcome to ' + txt +
                              '. \n\nTo register yourself, please send the last 4 characters of NRIC')
    return 'register_01'


def msg_register(update, context):
    update.message.reply_text(text='Hi, please enter your last 4 Characters Of NRIC.')
    return 'register_02'


def msg_add(update, context):
    txt = ''
    if TARGET_USER == 'main':
        txt = 'run xx, upper xx, lower xx...'
    elif TARGET_USER == 'trainees':
        txt = 'run xx, push up xx, sit up xx...'

    msg = ('Please enter your completed exercises. In this format:\n\n'
           + txt + '\n\n'
           'For more information on the format or backdate, use the following command: /help')
    update.message.reply_text(text=msg)
    return 'add_data'


def msg_del(update, context):
    msg = ('Enter date in DD/MM/YY format to delete exercises of the selected date.')
    update.message.reply_text(text=msg, parse_mode='HTML')
    return 'del_data'


def msg_get(update, context):
    msg = ('Enter date in DD/MM/YY format to get exercises done of the selected date.')
    update.message.reply_text(text=msg, parse_mode='HTML')
    return 'get_data'


def myinfo_wk(update, context):
    tele_id = update.message.from_user.id
    msg_out = myinfo('W-Mon', tele_id, TARGET_USER)
    update.message.reply_text(text=msg_out, parse_mode='HTML')


def myinfo_mth(update, context):
    tele_id = update.message.from_user.id
    msg_out = myinfo('M', tele_id, TARGET_USER)
    # msg_out += '\n\nTo view weekly info, use the following command: /myinfo_wk'
    update.message.reply_text(text=msg_out, parse_mode='HTML')


def myinfo_total(update, context):
    tele_id = update.message.from_user.id
    msg_out = myinfo('AS', tele_id, TARGET_USER)
    update.message.reply_text(text=msg_out, parse_mode='HTML')


def top5(update, context):
    if TARGET_USER == 'main':
        p_list = ['FullBodyPoints', 'UpperBodyPoints', 'LowerBodyPoints',
                  'Running_JoggingPoints', 'CyclingPoints', 'SwimmingPoints', 'TotalPoints']
        conn = sqlite3.connect('sig360.db')
        df = pd.read_sql_query(f"SELECT * FROM Sig_360_main;", conn)
        conn.close()
        df['DateOfExercise'].replace('', np.nan, inplace=True)
        df['DateOfExercise'] = pd.to_datetime(df['DateOfExercise'], dayfirst=True, format='%d-%b-%Y')
        df[p_list] = df[p_list].apply(pd.to_numeric)
        # cap daily TotalPoints to 10
        df = df.groupby(['Last4CharactersOfNRIC', 'Nickname', 'DateOfExercise'], as_index=False)[p_list].sum()
        df['TotalPoints'] = df['TotalPoints'].apply(lambda x: x if x < 10 else 10)
    elif TARGET_USER == 'trainees':
        df = df_trainees_points()

    df = df.groupby(['Last4CharactersOfNRIC', 'Nickname']).sum()
    df.sort_values(by='TotalPoints', ascending=False, inplace=True)
    df.reset_index(inplace=True)
    df = df.head(5)

    msg_out = '<b>Top 5 Scorers</b>:\n\n'
    for i in range(0, df.shape[0]):
        msg_out += f"{df.at[i,'Nickname']} - {int(df.at[i,'TotalPoints'])}\n"

    update.message.reply_text(text=msg_out, parse_mode='HTML')


def msg_help(update, context):
    msg_out = ("<b>Keyword</b>:\nrun = Running (Km)\nfull = Full Body Exercise\nupper = Upper Body Exercise\n"
               "lower = Lower Body Exercise\ncycle = Cycling (Km)\nswim = Swimming (Km)\nwalk = Walking (Km)\n\n"
               "<b>Example message</b>:\nrun 5, upper 60, lower 60, full 50\n\n"
               "To backdate, enter date in DD/MM/YY follow by completed exercises.\n\n<b>Example message</b>:\n"
               "16/10/20\nrun 5, upper 60, lower 60, full 50"
               )
    update.message.reply_text(text=msg_out, parse_mode='HTML')


def add_user_to_db(update, context):
    try:
        nric = update.message.text
        nric = nric.upper()
        tele_id = update.message.from_user.id
        if check_nric(nric):
            conn = sqlite3.connect('sig360.db')
            sql_nric = pd.read_sql_query(
                f"SELECT Last4CharactersOfNRIC_eg123A FROM Registration WHERE UPPER(Last4CharactersOfNRIC_eg123A)='{nric}';", conn)
            conn.close()
            my_list = [nric, tele_id]
            task_add_tele_id.delay(my_list, TARGET_USER)
            update.message.reply_text(
                text="Great! I've registered you into the system.\n\nTo update your workouts, use the following command: /add")
        else:
            update.message.reply_text(text="You have enter invalid format.")
    except Exception as e:
        update.message.reply_text(
            text="Error. You need to register through the Google Form before using this services.")


def add_data_to_db(update, context):
    input_v = update.message.text
    input_v = input_v.lower()
    input_list = input_v.split(',')
    tele_id = update.message.from_user.id

    conn = sqlite3.connect('sig360.db')
    nric_n_nick = pd.read_sql_query(f"SELECT Last4CharactersOfNRIC_eg123A, Nickname FROM Registration WHERE TeleID={tele_id};", conn).values[0]

    if TARGET_USER == 'main':
        df = pd.DataFrame(columns=main_full_list)
    elif TARGET_USER == 'trainees':
        df = pd.DataFrame(columns=trainees_full_list)
    conn.close()

    try:
        df = input_to_df(df, input_list, nric_n_nick, TARGET_USER)
        task_add_data.delay(df.to_json(), TARGET_USER)
        update.message.reply_text(
            text="Updating in progress. Your results will be reflected in your progress after a few minutes.")
    except Exception as e:
        print("Data Error:" + str(e))
        update.message.reply_text(text="You have enter invalid format.")


def del_data_from_db(update, context):
    input_v = update.message.text
    try:
        input_v_dt = datetime.datetime.strptime(input_v, '%d/%m/%y')
        input_v = input_v_dt.strftime('%-m/%-d/%Y')
        tele_id = update.message.from_user.id

        conn = sqlite3.connect('sig360.db')
        nric_n_nick = pd.read_sql_query(
            f"SELECT Last4CharactersOfNRIC_eg123A, Nickname FROM Registration WHERE TeleID={tele_id};", conn).values[0]
        select_df_by_date_nric = pd.read_sql_query(
            f"SELECT * FROM Sig_360_main WHERE DateOfExercise = '{input_v}' AND UPPER(Last4CharactersOfNRIC) = '{nric_n_nick[0].upper()}';", conn)
        select_df_by_date_nric.reset_index(inplace=True)

        task_del_data.delay(select_df_by_date_nric.to_json(), TARGET_USER)
        update.message.reply_text(text="Delete request has been sent. Your entry will be deleted in several minutes")
    except Exception as e:
        update.message.reply_text(text="Wrong date format or record does not exist.")


def get_data_from_db(update, context):
    if TARGET_USER == 'main':
        df_list = main_full_list[3:10]
    elif TARGET_USER == 'trainees':
        df_list = trainees_full_list[2:8]

    input_v = update.message.text
    try:
        input_v_dt = datetime.datetime.strptime(input_v, '%d/%m/%y')
        input_v = input_v_dt.strftime('%-m/%-d/%Y')
        tele_id = update.message.from_user.id

        conn = sqlite3.connect('sig360.db')
        nric_n_nick = pd.read_sql_query(
            f"SELECT Last4CharactersOfNRIC_eg123A, Nickname FROM Registration WHERE TeleID={tele_id};", conn).values[0]

        select_df_by_date_nric = pd.read_sql_query(
            f"SELECT * FROM Sig_360_main WHERE DateOfExercise = '{input_v}' AND UPPER(Last4CharactersOfNRIC) = '{nric_n_nick[0].upper()}';", conn)
        select_df_by_date_nric = select_df_by_date_nric.head(1)
        select_df_by_date_nric = select_df_by_date_nric[df_list]
        select_df_by_date_nric[df_list] = select_df_by_date_nric[df_list].apply(pd.to_numeric)
        select_df_by_date_nric.drop(['index'], axis=1, errors='ignore', inplace=True)

        input_v = input_v_dt.strftime('%-d/%-m/%Y')

        msg_out = f"<b>Completed exercises on {input_v}</b>:\n"
        for i in select_df_by_date_nric:
            if pd.notna(select_df_by_date_nric.at[0, i]):
                msg = f"{i}: {select_df_by_date_nric.at[0,i]}"
                if '_km' in i.lower():
                    msg = msg.replace('_', '')
                    msg = msg.replace('Km', '')
                    msg = msg.replace('KM', '')
                    msg += ' Km'
                else:
                    msg += ' reps'

                msg = msg.replace('or', '/')
                msg = msg.replace('Or', '/')
                msg = re.sub(r"(\w)([A-Z])", r"\1 \2", msg)
                msg_out += msg + "\n"

        if msg_out == f"<b>Completed exercises on {input_v}</b>:\n":
            msg_out += 'no record'

        update.message.reply_text(text=msg_out, parse_mode='HTML')
    except Exception as e:
        update.message.reply_text(text="Wrong date format or record does not exist.")


def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def run_telegram_bot():
    if TARGET_USER == 'main':
        updater = Updater("620407103:AAHKzf5SC0qZTYmYeHGMGP3n4MY8pa7CXNg", use_context=True)
    elif TARGET_USER == 'trainees':
        updater = Updater("825078881:AAER7Q7D63gQwFC5SvRkMRSxtOk_PoRmc4w", use_context=True)

    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[
            # RegexHandler('(?i)(?=.*summary).{10,}', summary),
            CommandHandler('start', msg_start),
            CommandHandler('register', msg_register),
            CommandHandler('add', msg_add),
            # CommandHandler('myinfo_mth', myinfo_mth),
            # CommandHandler('myinfo_total', myinfo_total),
            # CommandHandler('top5', top5),
            CommandHandler('get', msg_get),
            CommandHandler('delete', msg_del),
            CommandHandler('help', msg_help)
        ],

        states={
            'register_01': [MessageHandler(Filters.text, add_user_to_db)],
            'register_02': [MessageHandler(Filters.text, add_user_to_db)],
            'add_data': [MessageHandler(Filters.text, add_data_to_db)],
            'get_data': [MessageHandler(Filters.text, get_data_from_db)],
            'del_data': [MessageHandler(Filters.text, del_data_from_db)]
        },

        fallbacks=[MessageHandler(Filters.text, error)],

        allow_reentry=True
    )

    dp.add_handler(conv_handler)
    dp.add_error_handler(error)

    updater.start_polling()
    # updater.idle()


if __name__ == '__main__':
    task_up_all_db.delay(TARGET_USER)

    thread1 = Thread(target=run_telegram_bot)
    thread2 = Thread(target=run_celery)

    try:
        thread1.start()
        thread2.start()
    except Exception as e:
        thread1.stop()
        thread2.stop()
        print('Arrr ERROR')


'''# advance feature. temporarily suspended
def report (update, context):
    conn = sqlite3.connect('sig360.db')
    tele_id = update.message.from_user.id
    nric_n_nick = pd.read_sql_query(f"SELECT Last4CharactersOfNRIC_eg123A, Nickname FROM Registration WHERE TeleID={tele_id};", conn).values[0]
    df = pd.read_sql_query(f"SELECT Date, TotalPoints FROM Sig_360_main WHERE LOWER(Last4CharactersOfNRIC)='{nric_n_nick[0]}';", conn)
    conn.close()

    top_txt = f"{nric_n_nick[1]}"
    bottom_txt = 'hello world 2'
    
    df['Date'] = pd.to_datetime(df['Date'], dayfirst=True, format='%d %B %Y')
    df = df.resample('W-Mon', on='Date').sum().reset_index().sort_values(by='Date')
    df = df.tail(6)

    img = report_card_image(top_txt, bottom_txt, nric_n_nick[0], df)
    update.message.reply_photo(photo=io.BytesIO(img))
'''

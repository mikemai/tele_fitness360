import datetime
import numpy as np
import pandas as pd
import pytz
import sqlite3
import time

from celery import Celery
from celery.schedules import crontab

from ex_function import (trainees_full_list, rename_df_col_for_db, update_to_df, client, refresh_creds)

app = Celery('tasks', broker='sqla+sqlite:///celery.db', backend='db+sqlite:///celery_results.db')
SHEET_ID = '1wxTgsCWekmG6OjSZoofuKy2Sea65GG4Sgbnku2EAf-0'
S_TIMER = 10


app.conf.beat_schedule = {
    "update-db-task": {
        "task": "tasks.task_up_all_db",
        "schedule": crontab(minute=0, hour=0)
    }
}


@app.task
def task_up_all_db(target_user):
    refresh_creds()
    conn = sqlite3.connect('sig360.db')
    if target_user == 'main':
        SHEET_ID = '1wxTgsCWekmG6OjSZoofuKy2Sea65GG4Sgbnku2EAf-0'
    elif target_user == 'trainees':
        SHEET_ID = '1KMIU8NrfOyN_TnUoJWQrSFHEuwIWH8k9Q-ZSC9aKNXc'

    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'Registration')
    df = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    if target_user == 'trainees':
        df.rename(columns={'Last4charactersofNRIC_eg123A': 'Last4CharactersOfNRIC_eg123A'}, inplace=True)
    df.to_sql(name='Registration', con=conn, if_exists='replace')

    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'Data')
    df = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    df_new_timestamp = df["Timestamp"].str.split(" ", n=1, expand=True)
    df.loc[df["DateOfExercise"] == '', 'DateOfExercise'] = df_new_timestamp[0]
    if target_user == 'trainees':
        df.rename(columns={'NRIC_Last4character_eg123A': 'Last4CharactersOfNRIC',
                           'Timestamp': 'DateOfExercise'}, inplace=True)
    df.to_sql(name='Sig_360_main', con=conn, if_exists='replace')

    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'SIChallenge')
    df = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    df.to_sql(name='SIChallenge', con=conn, if_exists='replace')

    conn.close()
    time.sleep(S_TIMER)


@app.task
def task_add_data(json_df, target_user):
    refresh_creds()
    conn = sqlite3.connect('sig360.db')

    if target_user == 'main':
        SHEET_ID = '1wxTgsCWekmG6OjSZoofuKy2Sea65GG4Sgbnku2EAf-0'
    elif target_user == 'trainees':
        SHEET_ID = '1KMIU8NrfOyN_TnUoJWQrSFHEuwIWH8k9Q-ZSC9aKNXc'

    df = pd.read_json(json_df, convert_dates=False)
    s_date = df.at[0, 'DateOfExercise']
    nric = df.at[0, 'Last4CharactersOfNRIC']
    nric = nric.upper()
    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'Data')
    df_sheet = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    df_new_timestamp = df_sheet["Timestamp"].str.split(" ", n=1, expand=True)
    df_sheet.loc[df_sheet["DateOfExercise"] == '', 'DateOfExercise'] = df_new_timestamp[0]

    if df_sheet.empty:
        select_df_by_date_nric = pd.DataFrame()
        df_sheet = pd.DataFrame()
    else:
        select_df_by_date_nric = pd.read_sql_query(
            f"SELECT * FROM Sig_360_main WHERE DateOfExercise = '{s_date}' AND UPPER(Last4CharactersOfNRIC) = '{nric}';", conn)
        if target_user == 'trainees':
            df_sheet.rename(columns={'NRIC_Last4character_eg123A': 'Last4CharactersOfNRIC',
                                     'Timestamp': 'DateOfExercise'}, inplace=True)
            df_sheet = df_sheet[trainees_full_list]

    if select_df_by_date_nric.empty:
        print('----------------------  select_df_by_date_nric.empty  -----------------------')
        df_sheet = df.append(df_sheet, ignore_index=True, sort=False)
        # season 2. points are to be calculatated in excel instead
        season2_list = df.values.tolist()[0]
        season2_list = season2_list[0:10]
        sheet.insert_rows(1, values=season2_list)
    else:
        print('----------------------  select_df_by_date_nric.not empty  -----------------------')
        index, df_sheet = update_to_df(df_sheet, df, target_user)
        new_values = df_sheet.loc[index].values.tolist()
        for i, v in enumerate(new_values):
            if isinstance(v, np.int64):
                new_values[i] = float(v)
        # season 2. points are to be calculatated in excel instead
        new_values = new_values[0:10]
        sheet.update_row(index+2, new_values)

    df_sheet.to_sql(name='Sig_360_main', con=conn, if_exists='replace')

    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'SIChallenge')
    df = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    df.to_sql(name='SIChallenge', con=conn, if_exists='replace')

    conn.close()
    time.sleep(S_TIMER)


@app.task
def task_del_data(json_df, target_user):
    refresh_creds()
    conn = sqlite3.connect('sig360.db')
    today = datetime.datetime.now(pytz.timezone('Asia/Singapore')).date()
    if target_user == 'main':
        SHEET_ID = '1wxTgsCWekmG6OjSZoofuKy2Sea65GG4Sgbnku2EAf-0'
    elif target_user == 'trainees':
        SHEET_ID = '1KMIU8NrfOyN_TnUoJWQrSFHEuwIWH8k9Q-ZSC9aKNXc'

    df = pd.read_json(json_df, convert_dates=False)

    s_date = df.at[0, 'DateOfExercise']
    nric = df.at[0, 'Last4CharactersOfNRIC']
    nric = nric.upper()
    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'Data')
    df_sheet = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    df_new_timestamp = df_sheet["Timestamp"].str.split(" ", n=1, expand=True)
    df_sheet.loc[df_sheet["DateOfExercise"] == '', 'DateOfExercise'] = df_new_timestamp[0]
    if target_user == 'trainees':
        df_sheet.rename(columns={'NRIC_Last4character_eg123A': 'Last4CharactersOfNRIC',
                                 'Timestamp': 'DateOfExercise'}, inplace=True)

    if not df.empty:
        index = 0
        for i in range(0, df_sheet.shape[0]):
            main_date_of_exercise = df_sheet['DateOfExercise'].iloc[i]
            main_last_4char_nric = df_sheet['Last4CharactersOfNRIC'].iloc[i]
            main_last_4char_nric = main_last_4char_nric.upper()

            if s_date in main_date_of_exercise and nric in main_last_4char_nric:
                index = i
                break

        new_values = []
        if target_user == 'main':
            new_values = [today.strftime('%-m/%-d/%Y'), s_date, df.at[0, 'Last4CharactersOfNRIC'],
                          '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
        elif target_user == 'trainees':
            new_values = [today.strftime('%d-%b-%Y'), nric, '', '', '', '', '', '']
        df_sheet.loc[index] = new_values
        sheet.update_row(index+2, new_values)
        # df_sheet.drop([index], axis=0, inplace=True)

    df_sheet.to_sql(name='Sig_360_main', con=conn, if_exists='replace')

    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'SIChallenge')
    df = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    df.to_sql(name='SIChallenge', con=conn, if_exists='replace')

    conn.close()
    time.sleep(S_TIMER)


@app.task
def task_add_tele_id(my_list, target_user):
    refresh_creds()
    conn = sqlite3.connect('sig360.db')
    if target_user == 'main':
        SHEET_ID = '1wxTgsCWekmG6OjSZoofuKy2Sea65GG4Sgbnku2EAf-0'
    elif target_user == 'trainees':
        SHEET_ID = '1KMIU8NrfOyN_TnUoJWQrSFHEuwIWH8k9Q-ZSC9aKNXc'

    sheet = client.open_by_key(SHEET_ID).worksheet('title', 'Registration')
    df = rename_df_col_for_db(pd.DataFrame(sheet.get_all_records()))
    if target_user == 'trainees':
        df.rename(columns={'Last4charactersofNRIC_eg123A': 'Last4CharactersOfNRIC_eg123A'}, inplace=True)

    for i in range(0, df.shape[0]):
        nric = str(df['Last4CharactersOfNRIC_eg123A'].iloc[i])

        if nric != 'nan' and my_list[0] in nric.upper():
            df.at[i, 'TeleID'] = my_list[1]
            matrix_tele_id = []
            matrix_tele_id.append(my_list[1])
            i += 1
            sheet.update_col(df.columns.get_loc('TeleID')+1, matrix_tele_id, row_offset=i)

    df.to_sql(name='Registration', con=conn, if_exists='replace')
    conn.close()
    time.sleep(S_TIMER)

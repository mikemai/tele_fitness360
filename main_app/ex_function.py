# import io
# import matplotlib.pyplot as plt
# import seaborn as sns
# from PIL import Image, ImageDraw, ImageFont
import datetime
import numpy as np
import pandas as pd
import pytz
import pygsheets
import re
import sqlite3

from google.auth.transport.requests import Request
from google.oauth2 import service_account

# use creds to create a client to interact with the Google Drive API
scope = ['https://www.googleapis.com/auth/spreadsheets']
creds = service_account.Credentials.from_service_account_file(
    'service_account_credentials.json', scopes=scope)
client = pygsheets.authorize(custom_credentials=creds)

mth = {'01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May', '06': 'Jun',
       '07': 'Jul', '08': 'Aug', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec'}
trainees_full_list = ['DateOfExercise', 'Last4CharactersOfNRIC', 'PushUp', 'SitUporLegRaiseorFlutterKicks',
                      'SquatsorLunges', 'HeelRaises', 'ChinUp', 'RunningOrJogging_KM']
main_full_list = ['Timestamp', 'DateOfExercise', 'Last4CharactersOfNRIC', 'FullBodyExercise', 'UpperBodyExercise', 'LowerBodyExercise',
                  'Running_Km', 'Cycling_Km', 'Swimming_Km', 'Walking_Km', 'Nickname', 'FullBodyPoints', 'UpperBodyPoints',
                  'LowerBodyPoints', 'Running_JoggingPoints', 'CyclingPoints', 'SwimmingPoints', 'WalkingPoints', 'TotalPoints']


def refresh_creds():
    print('~~~~~~~~~~~~ run function refresh ~~~~~~~~~~~~')
    if creds and creds.expired:
        print('~~~~~~~~~~~~ creds expired and refresh_token ~~~~~~~~~~~~')
        creds.refresh(Request())
        client = pygsheets.authorize(custom_credentials=creds)


def rename_df_col_for_db(df):
    new_list_df = []
    for c in df.columns.values.tolist():
        c = c.replace(".", "")
        c = c.replace(" ", "")
        c = c.replace("/", "_")
        c = c.replace("(", "_")
        c = c.replace(")", "")
        new_list_df.append(c)
    df.columns = new_list_df
    return df


def input_to_df(df, input_list, nric_n_nick, target_user):
    # *google sheet note*
    # datetime uses serial number starting from datetime.date(1899,12,30)).days
    today = datetime.datetime.now(pytz.timezone('Asia/Singapore')).date()
    s_date = ''

    for i in df.columns.values.tolist():
        if 'points' in i.lower():
            df.at[0, i] = 0

    if '/' not in input_list[0].lower():
        s_date = today.strftime('%-m/%-d/%Y')
    else:
        i_date = input_list[0].split('\n')
        input_list[0] = i_date[1]
        s_date = datetime.datetime.strptime(i_date[0], '%d/%m/%y')
        s_date = s_date.strftime('%-m/%-d/%Y')

    for i in input_list:
        reps = re.findall('\d+\.?\d*', i)
        if len(reps) == 0:
            reps = 0
        else:
            reps = float(reps[0])

        if target_user == 'main':
            if 'full' in i.lower():
                df.at[0, main_full_list[3]] = reps
            elif 'upper' in i.lower():
                df.at[0, main_full_list[4]] = reps
            elif 'lower' in i.lower():
                df.at[0, main_full_list[5]] = reps
            elif 'run' in i.lower():
                df.at[0, main_full_list[6]] = reps
            elif 'cycl' in i.lower():
                df.at[0, main_full_list[7]] = reps
            elif 'swim' in i.lower():
                df.at[0, main_full_list[8]] = reps
            elif 'walk' in i.lower():
                df.at[0, main_full_list[9]] = reps
        if target_user == 'trainees':
            if 'push' in i.lower():
                df.at[0, trainees_full_list[2]] = reps
            elif 'sit' in i.lower() or 'leg' in i.lower() or \
                    'kick' in i.lower() or 'flutter' in i.lower():
                df.at[0, trainees_full_list[3]] = reps
            elif 'squat' in i.lower() or 'lunge' in i.lower():
                df.at[0, trainees_full_list[4]] = reps
            elif 'heel' in i.lower():
                df.at[0, trainees_full_list[5]] = reps
            elif 'chin' in i.lower():
                df.at[0, trainees_full_list[6]] = reps
            elif 'run' in i.lower() or 'jog' in i.lower():
                df.at[0, trainees_full_list[7]] = reps

    df = df.fillna('')
    df.at[0, 'DateOfExercise'] = s_date
    df.at[0, 'Last4CharactersOfNRIC'] = nric_n_nick[0].upper()

    if target_user == 'main':
        df.at[0, 'Timestamp'] = today.strftime('%-m/%-d/%Y')
        df.at[0, 'Nickname'] = nric_n_nick[1]
        df = calculate_points(df)

    return df


def update_to_df(main_df, input_df, target_user):
    selected_date_of_exercise = input_df['DateOfExercise'].iloc[0]
    selected_nric = input_df['Last4CharactersOfNRIC'].iloc[0]
    selected_nric = selected_nric.upper()
    index = 0

    conn = sqlite3.connect('sig360.db')
    nick = pd.read_sql_query(
        f"SELECT Nickname FROM Registration WHERE UPPER(Last4CharactersOfNRIC_eg123A) = '{selected_nric}';", conn).values[0][0]
    conn.close()

    for i in range(0, main_df.shape[0]):
        main_date_of_exercise = main_df['DateOfExercise'].iloc[i]
        main_last_4char_nric = main_df['Last4CharactersOfNRIC'].iloc[i]
        main_last_4char_nric = main_last_4char_nric.upper()

        if selected_date_of_exercise in main_date_of_exercise and selected_nric in main_last_4char_nric:
            main_df.at[i, 'Nickname'] = nick
            index = i

            for j in main_df.columns.values.tolist():
                if 'date' not in j.lower() and 'point' not in j.lower() and \
                        'nric' not in j.lower() and 'time' not in j.lower() and 'nick' not in j.lower():
                    m_df = main_df.at[i, j]
                    i_df = input_df.at[0, j]

                    if isinstance(m_df, str):
                        m_df = 0
                    if isinstance(i_df, str):
                        i_df = 0

                    m_df += i_df

                    if m_df == 0 and 'point' not in j.lower():
                        main_df.at[i, j] = ''
                    else:
                        def rz(x): return int(x) if x % 1 == 0 else x
                        main_df.at[i, j] = rz(m_df)

    if target_user == 'main':
        main_df = calculate_points(main_df, index)
    return index, main_df


def myinfo(period, tele_id, target_user):
    today_mth = datetime.datetime.now(pytz.timezone('Asia/Singapore')).date()

    conn = sqlite3.connect('sig360.db')
    nric_n_nick = pd.read_sql_query(f"SELECT Last4CharactersOfNRIC_eg123A, Nickname FROM Registration WHERE TeleID = {tele_id};", conn).values[0]
    total_register = str(pd.read_sql_query("SELECT COUNT(Last4charactersofnric_eg123a) FROM Registration;", conn).values[0][0])
    si_challenge = pd.read_sql_query(f"SELECT c1 FROM SIChallenge;", conn)
    df = pd.read_sql_query(f"SELECT * FROM Sig_360_main;", conn)
    conn.close()

    if target_user == 'main':
        p_list = ['FullBodyPoints', 'UpperBodyPoints', 'LowerBodyPoints', 'Running_JoggingPoints', 'CyclingPoints',
                  'SwimmingPoints', 'WalkingPoints', 'TotalPoints']
        df['DateOfExercise'] = pd.to_datetime(df['DateOfExercise'], dayfirst=True, format='%m/%d/%Y')
        df[p_list] = df[p_list].apply(pd.to_numeric)
        # cap daily TotalPoints to 10
        df = df.groupby(['Nickname', 'DateOfExercise'], as_index=False)[p_list].sum()
        df['TotalPoints'] = df['TotalPoints'].apply(lambda x: x if x < 10 else 10)
        df = df.groupby(['Nickname']).resample(period, on='DateOfExercise').sum().sort_values(by='DateOfExercise', ascending=False)
        df = df[df['TotalPoints'] > 0]
        df.reset_index(inplace=True)

        if period == 'M':
            next_mth = today_mth + datetime.timedelta(weeks=5)
            next_mth = pd.Timestamp(next_mth)
            df = df[df['DateOfExercise'] < next_mth]
            df.reset_index(drop=True, inplace=True)

    elif target_user == 'trainees':
        df = df_trainees_points()
        df = df.groupby(['Nickname']).resample(period, on='DateOfExercise').sum(
        ).sort_values(by='DateOfExercise', ascending=False)
        df.reset_index(inplace=True)

    df_myinfo_x = df[df['Nickname'] == nric_n_nick[1]]
    df_myinfo_x = df_myinfo_x.head(1)
    df_myinfo_x.reset_index(inplace=True)

    df_myinfo_y = df.groupby(['Nickname']).sum()
    df_myinfo_y.sort_values(by='TotalPoints', ascending=False, inplace=True)
    df_myinfo_y.reset_index(inplace=True)
    current_index = df_myinfo_y[df_myinfo_y['Nickname'] == nric_n_nick[1]].index[0]

    msg_out = ''
    txt_max = ''

    if period == 'M':
        today_mth = today_mth.strftime('%b-%y')
        if df_myinfo_x.at[0, 'DateOfExercise'].strftime('%b-%y') == today_mth:
            msg_out = f"<b>Total points for the Month of {df_myinfo_x.at[0,'DateOfExercise'].strftime('%b-%y')}</b>:\n\n"

            if int(df_myinfo_x.at[0, 'TotalPoints']) > 210 and target_user == 'main':
                txt_max = "  💪 <b>*Reaching MAXIMUM points!</b>"

            msg_out += f"Total Points: {int(df_myinfo_x.at[0,'TotalPoints'])}" + txt_max + "\n"
            for i in df_myinfo_x:
                if target_user == 'main':
                    if 'Point' in i and 'Total' not in i and df_myinfo_x.at[0, i] >= 1:
                        msg = f"{i}: {int(df_myinfo_x.at[0,i])}"
                        msg = msg.replace('Points', '')
                        msg = msg.replace('_', '/')
                        msg_out += msg + '\n'
                elif target_user == 'trainees':
                    if 'Point' in i and 'Total' not in i and df_myinfo_x.at[0, i] >= 1:
                        msg = f"{i}: {int(df_myinfo_x.at[0,i])}"
                        msg = msg.replace('Points', '')
                        msg = msg.replace('_', '/')
                        msg_out += msg + '\n'
        else:
            msg_out = f"<b>Total points for the Month of {today_mth}</b>:\n\nNo exercises record\n"

    elif period == 'AS':
        msg_out = f"<b>Total points for Season 2</b>:\n\n"
        msg_out += f"Total Points: {int(df_myinfo_x.at[0,'TotalPoints'])}" + txt_max + "\n"
        for i in df_myinfo_x:
            if target_user == 'main':
                if 'Point' in i and 'Total' not in i and df_myinfo_x.at[0, i] >= 1:
                    msg = f"{i}: {int(df_myinfo_x.at[0,i])}"
                    msg = msg.replace('Points', '')
                    msg = msg.replace('_', '/')
                    msg_out += msg + '\n'
            elif target_user == 'trainees':
                if 'Point' in i and 'Total' not in i and df_myinfo_x.at[0, i] >= 1:
                    msg = f"{i}: {int(df_myinfo_x.at[0,i])}"
                    msg = msg.replace('Points', '')
                    msg = msg.replace('_', '/')
                    msg_out += msg + '\n'

    statics = int(float(si_challenge.at[1, "c1"]))
    cardio = int(float(si_challenge.at[2, "c1"]))

    msg_out += f"\nYou are currently ranked at {current_index+1} out of {total_register} places. "
    msg_out += f"Total points for this season are {int(df_myinfo_y.at[current_index, 'TotalPoints'])}.\n"
    msg_out += f"\n\n🌟 Static Reps Challenge 🌟\n"
    msg_out += f"Total Static Reps: {statics:,}\n"
    msg_out += f"--> {3000000-statics:,} more to hit 3,000,000 target."
    msg_out += f"\n\n🌟 Cardio (Km) Challenge 🌟\n"
    msg_out += f"Total Distance Cover: {cardio:,} Km\n"
    msg_out += f"--> {100000-cardio:,} more to hit 100,000 target."

    return msg_out


def calculate_points(df, index=0):
    t_df = df.iloc[[index]]
    t_df.reset_index(inplace=True)

    ex_list = main_full_list[3:10]
    p_list = main_full_list[11:19]
    total_point = 0

    print(ex_list)
    print(p_list)

    for v in ex_list:
        if isinstance(t_df.at[0, v], str):
            t_df.at[0, v] = 0

    full_p = int(t_df.at[0, ex_list[0]]/20)
    upper_p = int(t_df.at[0, ex_list[1]]/20)
    lower_p = int(t_df.at[0, ex_list[2]]/20)
    run_p = int(t_df.at[0, ex_list[3]])
    cycl_p = int(t_df.at[0, ex_list[4]]/4)
    swim_p = int(t_df.at[0, ex_list[5]]/0.2)
    walk_p = int(t_df.at[0, ex_list[6]])

    df.at[index, p_list[0]] = full_p
    df.at[index, p_list[1]] = upper_p
    df.at[index, p_list[2]] = lower_p
    df.at[index, p_list[3]] = run_p
    df.at[index, p_list[4]] = cycl_p
    df.at[index, p_list[5]] = swim_p
    df.at[index, p_list[6]] = walk_p

    total_point = full_p + upper_p + lower_p + run_p + cycl_p + swim_p + walk_p
    df.at[index, 'TotalPoints'] = if_more_than_10_is_10(total_point)

    return df


def if_more_than_10_is_10(input_v):
    if input_v > 10:
        input_v = 10

    return int(input_v)


def check_nric(input_v):
    bool_check = False

    if len(input_v[:-1]) == 3 and input_v[:-1].isdigit() \
            and input_v[-1:].isalpha():
        bool_check = True

    return bool_check


def df_trainees_points():
    ex_list = trainees_full_list[2:]
    conn = sqlite3.connect('sig360.db')
    df = pd.read_sql_query("SELECT * FROM Sig_360_main;", conn)

    if not df.empty:
        nric_n_nick = pd.read_sql_query("SELECT Last4CharactersOfNRIC_eg123A, Nickname FROM Registration;", conn)
        df['DateOfExercise'].replace('', np.nan, inplace=True)
        df.dropna(subset=['DateOfExercise'], inplace=True)
        df['DateOfExercise'] = pd.to_datetime(df['DateOfExercise'], dayfirst=True, format='%d-%b-%Y')
        df[ex_list] = df[ex_list].apply(pd.to_numeric)
        df = df.groupby(['Last4CharactersOfNRIC', 'DateOfExercise'], as_index=False)[ex_list].sum()
        df['CardioPoints'] = df[ex_list[5]]*50
        df['StaticsPoints'] = df[ex_list[0]] + df[ex_list[1]] + df[ex_list[2]] + df[ex_list[3]] + df[ex_list[4]]*5
        df['TotalPoints'] = df['CardioPoints'] + df['StaticsPoints']
        df['Nickname'] = ''
        for i in range(0, df.shape[0]):
            for j in range(0, nric_n_nick.shape[0]):
                if df.at[i, 'Last4CharactersOfNRIC'] == nric_n_nick.at[j, 'Last4CharactersOfNRIC_eg123A']:
                    df.at[i, 'Nickname'] = nric_n_nick.at[j, 'Nickname']
    else:
        df['TotalPoints'] = 0
        df['Nickname'] = ''

    return df


'''
def report_card_image(top_txt, bot_txt, nric, df):
    img_top = Image.new('RGB', (100, 30), color = 'white')
    img_bot = Image.new('RGB', (100, 30), color = 'white')
    fnt = ImageFont.truetype('Calibri.ttf', 15)
    
    # Top image message
    d = ImageDraw.Draw(img_top)
    d.text((10,10), top_txt, font=fnt, fill=(0, 0, 0))

    # Bottom image message
    d = ImageDraw.Draw(img_bot)
    d.text((10,10), bot_txt, font=fnt, fill=(0, 0, 0))

    df.columns = ['Week', 'Total Points']

    buf = io.BytesIO()
    sns.set(style="whitegrid")
    g = sns.barplot(x='Week', y='Total Points', data=df)

    x_dates = df['Week'].dt.strftime('%m-%d').sort_values().unique()
    for i, p_label in enumerate(x_dates):
        for key in mth:
            if key in p_label[:2]:
                x_dates[i] = mth[key] + p_label[2:]
                break
    g.set_xticklabels(x_dates)

    plt.xlabel('')
    plt.ylabel('')
    plt.title('Total Points per Weeks')
    plt.savefig(buf, format='jpg')
    buf.seek(0)
    img_mid = Image.open(buf)

    imgs = [img_top, img_mid, img_bot]
    widths, heights = zip(*(i.size for i in imgs))

    max_width = max(widths)
    total_height = sum(heights)

    new_im = Image.new('RGB', (max_width, total_height), color = 'white')

    y_offset = 0
    for im in imgs:
        new_im.paste(im, (0,y_offset))
        y_offset += im.size[1]
    
    buf = io.BytesIO()
    new_im.save(buf, format='png')
    buf.seek(0)
    final_img = buf.getvalue()
    plt.clf()
    buf.close()

    return final_img
'''
